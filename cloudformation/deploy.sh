#!/bin/bash

if [ -z "$ENVIRONMENT" ]; then
    echo 'Please specify an environment name (ENVIRONMENT) as either development, staging, or production'
    exit 1
fi

if [ -z "$S3_BUCKET" ]; then
    echo 'Please specify an S3 bucket for CloudFormation artifacts (S3_BUCKET)'
    exit 1
fi

if [ -z "$STACK_NAME" ]; then
    echo 'Please specify a root stack name (STACK_NAME)'
    exit 1
fi

if [ -z "$ALARM_EMAIL" ]; then
    echo 'Please specify an alarm topic email (ALARM_EMAIL)'
    exit 1
fi

aws cloudformation package \
    --template-file master.yaml \
    --s3-bucket $S3_BUCKET \
    --output-template-file packaged.yaml

aws cloudformation deploy \
    --template-file packaged.yaml \
    --stack-name $STACK_NAME \
    --parameter-overrides Environment="$ENVIRONMENT" Prefix="$STACK_NAME" AlarmEmail="$ALARM_EMAIL" \
    --capabilities CAPABILITY_NAMED_IAM
