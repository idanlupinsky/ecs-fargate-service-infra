AWSTemplateFormatVersion: 2010-09-09
Description: Provisions VPC endpoints to keep services traffic fully in the Amazon network.

Parameters:
  VPC:
    Type: AWS::EC2::VPC::Id
    Description: Create VPC endpoints in this VPC

  PrivateSubnets:
    Type: List<AWS::EC2::Subnet::Id>
    Description: The private subnets in which to create the Logs and ECR VPC endpoints

  PrivateRouteTables:
    Type: String
    Description: The private route tables to associate with the S3 VPC endpoint

  LogsEndpointSecurityGroup:
    Type: AWS::EC2::SecurityGroup::Id
    Description: The security group to associate with the Cloudwatch Logs VPC endpoint

  ECREndpointSecurityGroup:
    Type: AWS::EC2::SecurityGroup::Id
    Description: The security group to associate with the ECR VPC endpoint

Resources:
  S3Endpoint:
    Type: AWS::EC2::VPCEndpoint
    Properties:
      VpcEndpointType: Gateway
      ServiceName: !Sub com.amazonaws.${AWS::Region}.s3
      VpcId: !Ref VPC
      RouteTableIds:
        Fn::Split:
          - ","
          - !Ref PrivateRouteTables

  LogsEndpoint:
    Type: AWS::EC2::VPCEndpoint
    Properties:
      VpcEndpointType: Interface
      ServiceName: !Sub com.amazonaws.${AWS::Region}.logs
      VpcId: !Ref VPC
      PrivateDnsEnabled: true
      SubnetIds: !Ref PrivateSubnets
      SecurityGroupIds:
        - !Ref LogsEndpointSecurityGroup

  ECREndpoint:
    Type: AWS::EC2::VPCEndpoint
    Properties:
      VpcEndpointType: Interface
      ServiceName: !Sub com.amazonaws.${AWS::Region}.ecr.dkr
      VpcId: !Ref VPC
      PrivateDnsEnabled: true
      SubnetIds: !Ref PrivateSubnets
      SecurityGroupIds:
        - !Ref ECREndpointSecurityGroup

Outputs:
  S3Endpoint:
    Description: A reference to the S3 gateway endpoint
    Value: !Ref S3Endpoint

  ECREndpoint:
    Description: A reference to the ECR interface endpoint
    Value: !Ref ECREndpoint

  LogsEndpoint:
    Description: A reference to the Cloudwatch logs interface endpoint
    Value: !Ref LogsEndpoint
